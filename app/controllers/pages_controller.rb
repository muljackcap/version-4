class PagesController < ApplicationController
  
  def home
  	@title = "Home"
  end

  def sch
  	@title = "Schema"
  	render template: "pages/schema.html.erb"
  end

  def mid
  	@title = "Mid-Semester Deliv."
  	render template: "pages/mid.html.erb"
  end

  def final
    @title = "Final Deliverables"
    render template: "pages/final.html.erb"
  end
  
end
