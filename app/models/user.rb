class User < ApplicationRecord
  has_many :watchlists
  has_many :movie_ratings

  has_many :movies, through: :watchlists, dependent: :destroy
  has_many :ratings, through: :movie_ratings, dependent: :destroy

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  serialize :permissions, Hash
  serialize :watchlist, Array

  PERMISSIONS = [
  	"Registered",
    "Manager",
  	"Admin"
  ]

  def is?(param)
	  if PERMISSIONS.include?(param) # continue
	    permissions[param] ? true : false
	  else # this is bad, raise
	    raise "User cannot be #{param}"
	  end
	end

end
