class Movie < ApplicationRecord
	has_many :movie_genres
	has_many :movie_tags
	has_many :movie_ratings
	has_many :movie_crews
	has_many :watchlists
	
	has_many :genres, through: :movie_genres, dependent: :destroy
	has_many :tags, through: :movie_tags, dependent: :destroy
	has_many :ratings, through: :movie_ratings, dependent: :destroy
	has_many :crews, through: :movie_crews, dependent: :destroy
	has_many :users, through: :watchlists, dependent: :destroy



end
