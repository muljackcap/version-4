class Role < ApplicationRecord
	has_many :crew_roles

	has_many :crews, through: :crew_roles, dependent: :destroy
end
