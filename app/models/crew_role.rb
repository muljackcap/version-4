class CrewRole < ApplicationRecord
  belongs_to :crew
  belongs_to :role
end
