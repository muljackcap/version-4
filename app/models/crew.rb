class Crew < ApplicationRecord
	has_many :crew_roles
	has_many :movie_crews

	has_many :roles, through: :crew_roles, dependent: :destroy
	has_many :movies, through: :movie_crews, dependent: :destroy
end
