json.extract! movie, :id, :title, :summary, :release, :language, :duration, :image, :video, :created_at, :updated_at
json.url movie_url(movie, format: :json)
