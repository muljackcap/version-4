# README


CS405G Introduction to Database Systems – Course Project
Almost all of our editing will be in the 'app' directory.
NOTE - Please Read!
1
Getting Started with our Ruby on Rails project. Please use with a Linux or Unix shell.
You must have a local copy of mysql installed. Instructions can be found here: https://dev.mysql.com/doc/mysql-getting-started/en/
Step 1: Click the Clone link to the upper left below the project logo. Step 2: Paste the link into your shell and run the command. Step 3: Run '$ bundle install' Step 4: Edit the "username: " and "password" field in 'moviebase/moviebase/config/database.yml' file to reflect the username and password associated with your 'mysql' install. Step 5: Run '$ rails s'
You may need to run the command '$ rake db:create' or '$ rake db:migrate' to clear away the ActiveRecord Red Screen error.
If you receive an error for the Gem 'Uglifier', in a Unix or Linux shell or terminal run '$ sudo apt-get install nodejs' , then re-run '$ rails server'
No you should be set up and running a server on port 3000 (localhost:3000 in any web browser). Press 'Ctrl'+'c' to stop running the server at anytime.


This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...