class CreateMovies < ActiveRecord::Migration[5.0]
  def change
    create_table :movies do |t|
      t.string :title
      t.text :summary
      t.integer :release
      t.string :language
      t.integer :duration
      t.string :image
      t.string :video

      t.timestamps
    end
  end
end
