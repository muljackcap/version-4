class CreateCrewRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :crew_roles do |t|
      t.references :crew, foreign_key: true
      t.references :role, foreign_key: true

      t.timestamps
    end
  end
end
