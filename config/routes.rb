Rails.application.routes.draw do
  resources :roles
  resources :crews
  resources :ratings
  resources :tags
  resources :genres
  resources :movies
  devise_for :users, controllers: {sessions: "users/sessions", registrations: "users/registrations"}

  #, :skip => [:registrations]

  devise_scope :user do
    get 'edit_user_registration', to: 'users#edit'
  end
  resources :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'pages/home'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'movies#index'

  get '/schema' => 'pages#sch'
  get '/mid-semester' => 'pages#mid'
  get '/final' => 'pages#final'

end
