-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: version-4_development
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ar_internal_metadata`
--

DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ar_internal_metadata`
--

LOCK TABLES `ar_internal_metadata` WRITE;
/*!40000 ALTER TABLE `ar_internal_metadata` DISABLE KEYS */;
INSERT INTO `ar_internal_metadata` VALUES ('environment','development','2017-04-20 22:26:24','2017-04-20 22:26:24');
/*!40000 ALTER TABLE `ar_internal_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crew_roles`
--

DROP TABLE IF EXISTS `crew_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crew_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crew_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_crew_roles_on_crew_id` (`crew_id`),
  KEY `index_crew_roles_on_role_id` (`role_id`),
  CONSTRAINT `fk_rails_08701da723` FOREIGN KEY (`crew_id`) REFERENCES `crews` (`id`),
  CONSTRAINT `fk_rails_2087d09ff5` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crew_roles`
--

LOCK TABLES `crew_roles` WRITE;
/*!40000 ALTER TABLE `crew_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `crew_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `crews`
--

DROP TABLE IF EXISTS `crews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crews`
--

LOCK TABLES `crews` WRITE;
/*!40000 ALTER TABLE `crews` DISABLE KEYS */;
INSERT INTO `crews` VALUES (1,'Stephen Speilberg','2017-04-20 22:49:50','2017-04-20 22:49:50'),(2,'Ryan Reynolds','2017-04-20 22:50:16','2017-04-20 22:50:30'),(3,'Daniel Ratcliff','2017-04-20 22:50:39','2017-04-20 22:50:39'),(4,'Emma Watson','2017-04-20 22:50:47','2017-04-20 22:50:47'),(5,'Mitch Mullins','2017-04-20 22:50:54','2017-04-20 22:50:54'),(6,'Sam Caplan','2017-04-20 22:51:00','2017-04-20 22:51:00'),(7,'Matt Jackson','2017-04-20 22:51:26','2017-04-20 22:51:26'),(8,'Samuel L. Jackson','2017-04-20 22:51:38','2017-04-20 22:51:38'),(9,'Benedict Cumberbatch','2017-04-21 21:00:44','2017-04-21 21:00:44'),(10,'Christian Bale','2017-04-21 21:00:53','2017-04-21 21:00:53'),(11,'Eddie Murphy','2017-04-21 21:01:03','2017-04-21 21:01:03'),(12,'Kate Winslet','2017-04-21 21:01:13','2017-04-21 21:01:13'),(13,'Scarlett Johansson','2017-04-21 21:01:29','2017-04-21 21:01:29'),(14,'Jennifer Lawrence','2017-04-21 21:01:41','2017-04-21 21:01:41'),(15,'Jennifer Aniston','2017-04-21 21:01:50','2017-04-21 21:01:50'),(16,'Robert Downey Jr.','2017-04-21 21:02:02','2017-04-21 21:02:02'),(17,'Will Ferrel','2017-04-21 21:02:15','2017-04-21 21:02:15'),(18,'Steve Carell','2017-04-21 21:02:30','2017-04-21 21:02:30'),(19,'Tina Fey','2017-04-21 21:02:39','2017-04-21 21:02:39'),(20,'Will Arnett','2017-04-21 21:02:48','2017-04-21 21:02:48'),(21,'Leonardo DiCaprio','2017-04-21 21:03:01','2017-04-21 21:03:01'),(22,'Jamie Foxx','2017-04-21 21:03:12','2017-04-21 21:03:12'),(23,'Jude Law','2017-04-21 21:03:21','2017-04-21 21:03:21'),(24,'Stanley Cooper','2017-04-21 21:03:35','2017-04-21 21:03:35'),(25,'Michael Bay','2017-04-21 21:03:44','2017-04-21 21:03:44'),(26,'Quentin Tarantino','2017-04-21 21:04:03','2017-04-21 21:04:03'),(27,'Jerry Bruckheimer','2017-04-21 21:04:34','2017-04-21 21:04:34'),(28,'Martin Scorcese','2017-04-21 21:05:08','2017-04-21 21:05:08'),(29,'Martin Sheen','2017-04-21 21:05:18','2017-04-21 21:05:18'),(30,'Martin Freeman','2017-04-21 21:05:28','2017-04-21 21:05:28');
/*!40000 ALTER TABLE `crews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
INSERT INTO `genres` VALUES (1,'Kids','2017-04-20 22:39:48','2017-04-20 22:39:48'),(2,'Action','2017-04-20 22:39:54','2017-04-20 22:39:54'),(3,'Comedy','2017-04-20 22:40:02','2017-04-20 22:40:02'),(4,'Romance','2017-04-20 22:40:10','2017-04-20 22:40:10'),(5,'Romantic Comedy','2017-04-20 22:40:18','2017-04-20 22:40:18'),(6,'Fantasy','2017-04-20 22:40:30','2017-04-20 22:40:30'),(7,'Science Fiction','2017-04-20 22:41:55','2017-04-20 22:41:55'),(8,'Thriller','2017-04-20 22:42:14','2017-04-20 22:42:14'),(9,'Horror','2017-04-20 22:42:23','2017-04-20 22:42:28'),(10,'Drama','2017-04-21 13:22:38','2017-04-21 13:22:38'),(11,'Sports','2017-04-21 20:58:31','2017-04-21 20:58:31'),(12,'Documentary','2017-04-21 20:58:43','2017-04-21 20:58:43'),(13,'Historical fiction','2017-04-21 20:58:56','2017-04-21 20:58:56'),(14,'Musical','2017-04-21 20:59:32','2017-04-21 20:59:32'),(15,'Play-adaptation','2017-04-21 20:59:46','2017-04-21 20:59:46');
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_crews`
--

DROP TABLE IF EXISTS `movie_crews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_crews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) DEFAULT NULL,
  `crew_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_movie_crews_on_movie_id` (`movie_id`),
  KEY `index_movie_crews_on_crew_id` (`crew_id`),
  CONSTRAINT `fk_rails_35db488703` FOREIGN KEY (`crew_id`) REFERENCES `crews` (`id`),
  CONSTRAINT `fk_rails_816a3eba3e` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_crews`
--

LOCK TABLES `movie_crews` WRITE;
/*!40000 ALTER TABLE `movie_crews` DISABLE KEYS */;
INSERT INTO `movie_crews` VALUES (1,1,8,'2017-04-20 22:52:25','2017-04-20 22:52:25'),(4,3,1,'2017-04-21 03:56:04','2017-04-21 03:56:04'),(5,4,1,'2017-04-21 13:20:33','2017-04-21 13:20:33'),(6,7,2,'2017-04-21 13:23:54','2017-04-21 13:23:54'),(8,12,1,'2017-04-21 15:25:21','2017-04-21 15:25:21'),(9,12,3,'2017-04-21 15:25:21','2017-04-21 15:25:21'),(11,14,5,'2017-04-21 17:52:39','2017-04-21 17:52:39'),(12,15,6,'2017-04-21 18:01:16','2017-04-21 18:01:16'),(13,15,7,'2017-04-21 18:01:16','2017-04-21 18:01:16'),(14,15,8,'2017-04-21 18:01:16','2017-04-21 18:01:16'),(15,16,1,'2017-04-21 18:04:02','2017-04-21 18:04:02'),(16,16,4,'2017-04-21 18:04:02','2017-04-21 18:04:02'),(17,17,1,'2017-04-21 18:05:15','2017-04-21 18:05:15'),(18,18,5,'2017-04-21 18:07:14','2017-04-21 18:07:14'),(19,18,6,'2017-04-21 18:07:14','2017-04-21 18:07:14'),(20,18,8,'2017-04-21 18:07:14','2017-04-21 18:07:14'),(21,19,1,'2017-04-21 18:08:43','2017-04-21 18:08:43'),(22,19,2,'2017-04-21 18:08:43','2017-04-21 18:08:43'),(23,19,3,'2017-04-21 18:08:43','2017-04-21 18:08:43'),(24,19,4,'2017-04-21 18:08:43','2017-04-21 18:08:43'),(25,20,7,'2017-04-21 20:01:21','2017-04-21 20:01:21'),(26,21,1,'2017-04-21 20:02:28','2017-04-21 20:02:28'),(27,21,3,'2017-04-21 20:02:28','2017-04-21 20:02:28'),(28,22,1,'2017-04-21 20:04:02','2017-04-21 20:04:02'),(29,22,3,'2017-04-21 20:04:02','2017-04-21 20:04:02'),(30,22,4,'2017-04-21 20:04:02','2017-04-21 20:04:02'),(31,23,4,'2017-04-21 20:05:03','2017-04-21 20:05:03'),(32,23,6,'2017-04-21 20:05:03','2017-04-21 20:05:03'),(33,23,7,'2017-04-21 20:05:03','2017-04-21 20:05:03'),(34,24,2,'2017-04-21 20:07:21','2017-04-21 20:07:21'),(35,24,5,'2017-04-21 20:07:21','2017-04-21 20:07:21'),(36,24,8,'2017-04-21 20:07:21','2017-04-21 20:07:21'),(37,25,1,'2017-04-21 20:10:00','2017-04-21 20:10:00'),(38,25,5,'2017-04-21 20:10:00','2017-04-21 20:10:00'),(39,25,6,'2017-04-21 20:10:00','2017-04-21 20:10:00'),(40,25,7,'2017-04-21 20:10:00','2017-04-21 20:10:00'),(41,26,1,'2017-04-21 20:11:05','2017-04-21 20:11:05'),(42,26,4,'2017-04-21 20:11:05','2017-04-21 20:11:05'),(43,26,6,'2017-04-21 20:11:05','2017-04-21 20:11:05'),(44,26,8,'2017-04-21 20:11:05','2017-04-21 20:11:05'),(45,27,1,'2017-04-21 20:12:14','2017-04-21 20:12:14'),(46,27,8,'2017-04-21 20:12:14','2017-04-21 20:12:14'),(47,28,2,'2017-04-21 20:13:13','2017-04-21 20:13:13'),(48,28,3,'2017-04-21 20:13:13','2017-04-21 20:13:13'),(49,28,4,'2017-04-21 20:13:13','2017-04-21 20:13:13'),(50,29,8,'2017-04-21 20:14:29','2017-04-21 20:14:29'),(51,30,2,'2017-04-21 20:15:26','2017-04-21 20:15:26'),(52,31,3,'2017-04-21 20:16:40','2017-04-21 20:16:40'),(53,32,1,'2017-04-21 20:17:49','2017-04-21 20:17:49'),(54,32,4,'2017-04-21 20:17:49','2017-04-21 20:17:49'),(55,32,5,'2017-04-21 20:17:49','2017-04-21 20:17:49'),(56,33,1,'2017-04-21 20:18:45','2017-04-21 20:18:45'),(57,33,4,'2017-04-21 20:18:45','2017-04-21 20:18:45'),(58,34,2,'2017-04-21 20:19:43','2017-04-21 20:19:43'),(59,34,3,'2017-04-21 20:19:43','2017-04-21 20:19:43'),(60,34,5,'2017-04-21 20:19:43','2017-04-21 20:19:43'),(61,34,7,'2017-04-21 20:19:43','2017-04-21 20:19:43');
/*!40000 ALTER TABLE `movie_crews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_genres`
--

DROP TABLE IF EXISTS `movie_genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_movie_genres_on_movie_id` (`movie_id`),
  KEY `index_movie_genres_on_genre_id` (`genre_id`),
  CONSTRAINT `fk_rails_6f8a28d404` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`),
  CONSTRAINT `fk_rails_e153f3c39b` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_genres`
--

LOCK TABLES `movie_genres` WRITE;
/*!40000 ALTER TABLE `movie_genres` DISABLE KEYS */;
INSERT INTO `movie_genres` VALUES (1,1,1,'2017-04-20 22:52:25','2017-04-20 22:52:25'),(2,1,2,'2017-04-20 22:52:25','2017-04-20 22:52:25'),(5,3,2,'2017-04-21 03:56:04','2017-04-21 03:56:04'),(6,3,8,'2017-04-21 03:56:04','2017-04-21 03:56:04'),(7,4,7,'2017-04-21 13:20:33','2017-04-21 13:20:33'),(8,5,1,'2017-04-21 13:20:39','2017-04-21 13:20:39'),(9,5,3,'2017-04-21 13:20:39','2017-04-21 13:20:39'),(10,6,2,'2017-04-21 13:22:44','2017-04-21 13:22:44'),(11,6,7,'2017-04-21 13:22:44','2017-04-21 13:22:44'),(12,7,10,'2017-04-21 13:23:54','2017-04-21 13:23:54'),(13,8,2,'2017-04-21 13:25:11','2017-04-21 13:25:11'),(14,9,1,'2017-04-21 13:27:02','2017-04-21 13:27:02'),(15,9,3,'2017-04-21 13:27:02','2017-04-21 13:27:02'),(16,10,3,'2017-04-21 13:29:22','2017-04-21 13:29:22'),(19,12,7,'2017-04-21 15:25:21','2017-04-21 15:25:21'),(20,12,9,'2017-04-21 15:25:21','2017-04-21 15:25:21'),(22,14,2,'2017-04-21 17:52:39','2017-04-21 17:52:39'),(23,15,2,'2017-04-21 18:01:16','2017-04-21 18:01:16'),(24,16,2,'2017-04-21 18:04:02','2017-04-21 18:04:02'),(25,16,6,'2017-04-21 18:04:02','2017-04-21 18:04:02'),(26,16,7,'2017-04-21 18:04:02','2017-04-21 18:04:02'),(27,17,2,'2017-04-21 18:05:15','2017-04-21 18:05:15'),(28,17,8,'2017-04-21 18:05:15','2017-04-21 18:05:15'),(29,18,2,'2017-04-21 18:07:13','2017-04-21 18:07:13'),(30,18,8,'2017-04-21 18:07:13','2017-04-21 18:07:13'),(31,18,10,'2017-04-21 18:07:13','2017-04-21 18:07:13'),(32,19,8,'2017-04-21 18:08:43','2017-04-21 18:08:43'),(33,19,9,'2017-04-21 18:08:43','2017-04-21 18:08:43'),(34,20,8,'2017-04-21 20:01:21','2017-04-21 20:01:21'),(35,20,9,'2017-04-21 20:01:21','2017-04-21 20:01:21'),(36,21,8,'2017-04-21 20:02:28','2017-04-21 20:02:28'),(37,21,9,'2017-04-21 20:02:28','2017-04-21 20:02:28'),(38,22,8,'2017-04-21 20:04:02','2017-04-21 20:04:02'),(39,22,9,'2017-04-21 20:04:02','2017-04-21 20:04:02'),(40,23,8,'2017-04-21 20:05:03','2017-04-21 20:05:03'),(41,23,9,'2017-04-21 20:05:03','2017-04-21 20:05:03'),(42,24,3,'2017-04-21 20:07:21','2017-04-21 20:07:21'),(43,25,1,'2017-04-21 20:10:00','2017-04-21 20:10:00'),(44,25,3,'2017-04-21 20:10:00','2017-04-21 20:10:00'),(45,26,7,'2017-04-21 20:11:05','2017-04-21 20:11:05'),(46,26,10,'2017-04-21 20:11:05','2017-04-21 20:11:05'),(47,27,2,'2017-04-21 20:12:14','2017-04-21 20:12:14'),(48,28,4,'2017-04-21 20:13:13','2017-04-21 20:13:13'),(49,28,10,'2017-04-21 20:13:13','2017-04-21 20:13:13'),(50,29,2,'2017-04-21 20:14:29','2017-04-21 20:14:29'),(51,29,8,'2017-04-21 20:14:29','2017-04-21 20:14:29'),(52,30,3,'2017-04-21 20:15:26','2017-04-21 20:15:26'),(53,31,3,'2017-04-21 20:16:40','2017-04-21 20:16:40'),(54,32,10,'2017-04-21 20:17:49','2017-04-21 20:17:49'),(55,33,10,'2017-04-21 20:18:45','2017-04-21 20:18:45'),(56,34,3,'2017-04-21 20:19:42','2017-04-21 20:19:42');
/*!40000 ALTER TABLE `movie_genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_ratings`
--

DROP TABLE IF EXISTS `movie_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `movie_id` int(11) DEFAULT NULL,
  `rating_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_movie_ratings_on_user_id` (`user_id`),
  KEY `index_movie_ratings_on_movie_id` (`movie_id`),
  KEY `index_movie_ratings_on_rating_id` (`rating_id`),
  CONSTRAINT `fk_rails_6a16843a0d` FOREIGN KEY (`rating_id`) REFERENCES `ratings` (`id`),
  CONSTRAINT `fk_rails_e27a2e7bbc` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`),
  CONSTRAINT `fk_rails_fd3ebee10c` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_ratings`
--

LOCK TABLES `movie_ratings` WRITE;
/*!40000 ALTER TABLE `movie_ratings` DISABLE KEYS */;
INSERT INTO `movie_ratings` VALUES (1,1,NULL,1,'2017-04-21 00:03:20','2017-04-21 00:03:20'),(2,NULL,1,1,'2017-04-21 00:03:20','2017-04-21 00:03:20'),(16,NULL,3,22,'2017-04-21 14:37:35','2017-04-21 14:37:35'),(17,1,NULL,22,'2017-04-21 14:37:35','2017-04-21 14:37:35'),(18,NULL,4,23,'2017-04-21 14:38:56','2017-04-21 14:38:56'),(19,1,NULL,23,'2017-04-21 14:38:57','2017-04-21 14:38:57'),(20,NULL,5,24,'2017-04-21 14:39:58','2017-04-21 14:39:58'),(21,1,NULL,24,'2017-04-21 14:39:58','2017-04-21 14:39:58'),(22,NULL,6,25,'2017-04-21 14:44:01','2017-04-21 14:44:01'),(23,1,NULL,25,'2017-04-21 14:44:01','2017-04-21 14:44:01'),(24,NULL,7,26,'2017-04-21 14:44:33','2017-04-21 14:44:33'),(25,1,NULL,26,'2017-04-21 14:44:33','2017-04-21 14:44:33'),(26,NULL,8,27,'2017-04-21 14:45:11','2017-04-21 14:45:11'),(27,1,NULL,27,'2017-04-21 14:45:11','2017-04-21 14:45:11'),(28,NULL,9,28,'2017-04-21 14:45:28','2017-04-21 14:45:28'),(29,1,NULL,28,'2017-04-21 14:45:28','2017-04-21 14:45:28'),(30,NULL,10,29,'2017-04-21 14:45:58','2017-04-21 14:45:58'),(31,1,NULL,29,'2017-04-21 14:45:58','2017-04-21 14:45:58'),(32,NULL,1,30,'2017-04-21 14:46:25','2017-04-21 14:46:25'),(33,1,NULL,30,'2017-04-21 14:46:25','2017-04-21 14:46:25'),(34,NULL,7,31,'2017-04-21 14:49:51','2017-04-21 14:49:51'),(35,1,NULL,31,'2017-04-21 14:49:51','2017-04-21 14:49:51'),(36,NULL,12,32,'2017-04-21 15:26:15','2017-04-21 15:26:15'),(37,6,NULL,32,'2017-04-21 15:26:15','2017-04-21 15:26:15'),(38,NULL,14,33,'2017-04-21 21:06:31','2017-04-21 21:06:31'),(39,5,NULL,33,'2017-04-21 21:06:31','2017-04-21 21:06:31'),(40,NULL,15,34,'2017-04-21 21:07:02','2017-04-21 21:07:02'),(41,5,NULL,34,'2017-04-21 21:07:02','2017-04-21 21:07:02'),(42,NULL,16,35,'2017-04-21 21:07:34','2017-04-21 21:07:34'),(43,5,NULL,35,'2017-04-21 21:07:34','2017-04-21 21:07:34'),(44,NULL,17,36,'2017-04-21 21:07:59','2017-04-21 21:07:59'),(45,5,NULL,36,'2017-04-21 21:07:59','2017-04-21 21:07:59'),(46,NULL,18,37,'2017-04-21 21:08:34','2017-04-21 21:08:34'),(47,5,NULL,37,'2017-04-21 21:08:34','2017-04-21 21:08:34'),(48,NULL,19,38,'2017-04-21 21:08:57','2017-04-21 21:08:57'),(49,5,NULL,38,'2017-04-21 21:08:57','2017-04-21 21:08:57'),(50,NULL,20,39,'2017-04-21 21:09:30','2017-04-21 21:09:30'),(51,5,NULL,39,'2017-04-21 21:09:30','2017-04-21 21:09:30'),(52,NULL,21,40,'2017-04-21 21:09:51','2017-04-21 21:09:51'),(53,5,NULL,40,'2017-04-21 21:09:51','2017-04-21 21:09:51'),(54,NULL,22,41,'2017-04-21 21:10:11','2017-04-21 21:10:11'),(55,5,NULL,41,'2017-04-21 21:10:11','2017-04-21 21:10:11'),(56,NULL,23,42,'2017-04-21 21:10:36','2017-04-21 21:10:36'),(57,5,NULL,42,'2017-04-21 21:10:36','2017-04-21 21:10:36'),(58,NULL,24,43,'2017-04-21 21:11:06','2017-04-21 21:11:06'),(59,5,NULL,43,'2017-04-21 21:11:06','2017-04-21 21:11:06'),(60,NULL,25,44,'2017-04-21 21:11:30','2017-04-21 21:11:30'),(61,5,NULL,44,'2017-04-21 21:11:30','2017-04-21 21:11:30'),(62,NULL,26,45,'2017-04-21 21:11:51','2017-04-21 21:11:51'),(63,5,NULL,45,'2017-04-21 21:11:51','2017-04-21 21:11:51'),(64,NULL,27,46,'2017-04-21 21:12:30','2017-04-21 21:12:30'),(65,5,NULL,46,'2017-04-21 21:12:31','2017-04-21 21:12:31'),(66,NULL,28,47,'2017-04-21 21:13:02','2017-04-21 21:13:02'),(67,5,NULL,47,'2017-04-21 21:13:02','2017-04-21 21:13:02'),(68,NULL,29,48,'2017-04-21 21:13:29','2017-04-21 21:13:29'),(69,5,NULL,48,'2017-04-21 21:13:29','2017-04-21 21:13:29'),(70,NULL,30,49,'2017-04-21 21:13:54','2017-04-21 21:13:54'),(71,5,NULL,49,'2017-04-21 21:13:54','2017-04-21 21:13:54'),(72,NULL,31,50,'2017-04-21 21:14:20','2017-04-21 21:14:20'),(73,5,NULL,50,'2017-04-21 21:14:20','2017-04-21 21:14:20');
/*!40000 ALTER TABLE `movie_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_tags`
--

DROP TABLE IF EXISTS `movie_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_movie_tags_on_movie_id` (`movie_id`),
  KEY `index_movie_tags_on_tag_id` (`tag_id`),
  CONSTRAINT `fk_rails_2d5165d4b5` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`),
  CONSTRAINT `fk_rails_4d61dcda77` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_tags`
--

LOCK TABLES `movie_tags` WRITE;
/*!40000 ALTER TABLE `movie_tags` DISABLE KEYS */;
INSERT INTO `movie_tags` VALUES (4,3,2,'2017-04-21 03:56:04','2017-04-21 03:56:04'),(5,3,3,'2017-04-21 03:56:04','2017-04-21 03:56:04'),(6,1,3,'2017-04-21 05:49:01','2017-04-21 05:49:01'),(7,4,2,'2017-04-21 13:20:33','2017-04-21 13:20:33'),(8,4,3,'2017-04-21 13:20:33','2017-04-21 13:20:33'),(9,5,1,'2017-04-21 13:20:39','2017-04-21 13:20:39'),(10,5,4,'2017-04-21 13:20:39','2017-04-21 13:20:39'),(11,7,2,'2017-04-21 13:23:54','2017-04-21 13:23:54'),(12,7,3,'2017-04-21 13:23:54','2017-04-21 13:23:54'),(13,8,2,'2017-04-21 13:25:11','2017-04-21 13:25:11'),(15,6,6,'2017-04-21 14:43:31','2017-04-21 14:43:31'),(16,12,2,'2017-04-21 15:25:21','2017-04-21 15:25:21'),(19,14,2,'2017-04-21 17:52:39','2017-04-21 17:52:39'),(20,15,2,'2017-04-21 18:01:16','2017-04-21 18:01:16'),(21,15,3,'2017-04-21 18:01:16','2017-04-21 18:01:16'),(22,16,4,'2017-04-21 18:04:02','2017-04-21 18:04:02'),(23,16,6,'2017-04-21 18:04:02','2017-04-21 18:04:02'),(24,17,1,'2017-04-21 18:05:15','2017-04-21 18:05:15'),(25,18,2,'2017-04-21 18:07:13','2017-04-21 18:07:13'),(26,18,3,'2017-04-21 18:07:13','2017-04-21 18:07:13'),(27,19,5,'2017-04-21 18:08:43','2017-04-21 18:08:43'),(28,19,7,'2017-04-21 18:08:43','2017-04-21 18:08:43'),(29,20,7,'2017-04-21 20:01:21','2017-04-21 20:01:21'),(30,21,7,'2017-04-21 20:02:28','2017-04-21 20:02:28'),(31,22,7,'2017-04-21 20:04:02','2017-04-21 20:04:02'),(32,23,5,'2017-04-21 20:05:03','2017-04-21 20:05:03'),(33,23,7,'2017-04-21 20:05:03','2017-04-21 20:05:03'),(34,25,1,'2017-04-21 20:10:00','2017-04-21 20:10:00'),(35,26,2,'2017-04-21 20:11:05','2017-04-21 20:11:05'),(36,27,4,'2017-04-21 20:12:14','2017-04-21 20:12:14'),(37,27,5,'2017-04-21 20:12:14','2017-04-21 20:12:14'),(38,29,2,'2017-04-21 20:14:29','2017-04-21 20:14:29'),(39,30,1,'2017-04-21 20:15:26','2017-04-21 20:15:26'),(40,31,1,'2017-04-21 20:16:40','2017-04-21 20:16:40'),(41,32,7,'2017-04-21 20:17:49','2017-04-21 20:17:49'),(42,33,3,'2017-04-21 20:18:45','2017-04-21 20:18:45'),(43,34,1,'2017-04-21 20:19:43','2017-04-21 20:19:43');
/*!40000 ALTER TABLE `movie_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `summary` text,
  `release` int(11) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies`
--

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` VALUES (1,'Pokemon The Movie','The Best Movie ever Made!!!',1999,'English',120,'http://cdn-static.denofgeek.com/sites/denofgeek/files/2017/02/pokemon-main.jpg','https://www.youtube.com/watch?v=o0eUE5TmBaM','2017-04-20 22:52:25','2017-04-20 22:52:50'),(3,'Fast and Furious','Fast',2000,'English',120,'https://staticseekingalpha.a.ssl.fastly.net/uploads/2017/2/8476581_14864877962594_rId8.jpg','https://www.youtube.com/watch?v=NxhEZG0k9_w','2017-04-21 03:56:04','2017-04-21 03:56:04'),(4,'Avatar','Blue poeple fighting over a rock real good won oscars and stuff',2009,'English',182,'http://s3.foxmovies.com/foxmovies/production/films/18/images/posters/251-asset-page.jpg','https://www.youtube.com/watch?v=uZNHIU3uHT4','2017-04-21 13:20:33','2017-04-21 13:20:33'),(5,'Shrek 2','shrek is love shrek is life',2004,'english',93,'http://www.impawards.com/2004/posters/shrek_two_ver8.jpg','https://www.youtube.com/watch?v=1_uc1s3kLzo','2017-04-21 13:20:39','2017-04-21 13:20:39'),(6,'Star Wars: Episode III - Revenge of the Sith','luke turns into vader',2005,'eng',140,'https://upload.wikimedia.org/wikipedia/en/9/93/Star_Wars_Episode_III_Revenge_of_the_Sith_poster.jpg','https://www.youtube.com/watch?v=5UnjrG_N8hU','2017-04-21 13:22:44','2017-04-21 13:22:44'),(7,'The Dark Knight','Batman got dark voice and fought joker',2008,'English',173,'http://www.xpress.hu/dvd/cover/nagy/9982.jpg','https://www.youtube.com/watch?v=kmJLuwP3MbY','2017-04-21 13:23:54','2017-04-21 13:23:54'),(8,'Spider-Man 2','toby mcguire does it again!',2004,'english',127,'https://upload.wikimedia.org/wikipedia/en/0/02/Spider-Man_2_Poster.jpg','https://www.youtube.com/watch?v=BWsLc3j1AWg','2017-04-21 13:25:11','2017-04-21 13:25:11'),(9,'Space Jam','MJ and Bugs Bunny ball on dem folks',1996,'english',100,'https://upload.wikimedia.org/wikipedia/en/1/14/Space_jam.jpg','https://www.youtube.com/watch?v=oKNy-MWjkcU','2017-04-21 13:27:02','2017-04-21 13:27:02'),(10,'Anchor Man','I love lamp.',2004,'english',104,'https://upload.wikimedia.org/wikipedia/en/6/64/Movie_poster_Anchorman_The_Legend_of_Ron_Burgundy.jpg','https://www.youtube.com/watch?v=QQUi6Oi8SLQ','2017-04-21 13:29:22','2017-04-21 13:29:22'),(12,'Aliens','Stranded with an alien that eats people again',1986,'english',123,'https://upload.wikimedia.org/wikipedia/en/f/fb/Aliens_poster.jpg','https://www.youtube.com/watch?v=cN6E1j14ZUU','2017-04-21 15:25:21','2017-04-21 15:25:21'),(14,'Iron Man 3 ','Lots of Iron Men',2013,'English',140,'https://images-na.ssl-images-amazon.com/images/M/MV5BMTkzMjEzMjY1M15BMl5BanBnXkFtZTcwNTMxOTYyOQ@@._V1_UY1200_CR105,0,630,1200_AL_.jpg','https://www.youtube.com/watch?v=2CzoSeClcw0','2017-04-21 17:52:39','2017-04-21 17:52:39'),(15,'Transformers: Revenge of the Fallen','cars transform into robots and they fight stuff for the moon',2009,'English',150,'https://upload.wikimedia.org/wikipedia/en/thumb/c/cb/TF2SteelPoster.jpg/220px-TF2SteelPoster.jpg','https://www.youtube.com/watch?v=fnXzKwUgDhg','2017-04-21 18:01:16','2017-04-21 18:01:16'),(16,'Star Wars: Episode VII - The Force Awakens','lady jedi goes to luke to fight people',2015,'English',136,'https://s-media-cache-ak0.pinimg.com/736x/73/5d/a0/735da0d14c05527752cfc459bcfeeecc.jpg','https://www.youtube.com/watch?v=gAUxw4umkdY','2017-04-21 18:04:02','2017-04-21 18:04:02'),(17,'Jurassic World','chris pine wrstles some dinosaurs',2015,'English',124,'https://s-media-cache-ak0.pinimg.com/736x/0b/ab/9a/0bab9a9c671dbb7aa8626eec44a0195f.jpg','https://www.youtube.com/watch?v=RFinNxS5KN4','2017-04-21 18:05:15','2017-04-21 18:06:05'),(18,'Furious 7','why is the rock a good guy now',2015,'English',137,'https://images-na.ssl-images-amazon.com/images/I/51xJsYdMi8L.jpg','https://www.youtube.com/watch?v=D2NvoYKf7S0','2017-04-21 18:07:13','2017-04-21 18:07:43'),(19,'The Shining','Creepy Hotel',1980,'English',136,'http://static.rogerebert.com/uploads/movie/movie_poster/the-shining-1980/large_zc5y5OwKSV9MDXpfWxwrOjyRHsq.jpg','https://www.youtube.com/watch?v=HEew7zvpAWE','2017-04-21 18:08:43','2017-04-21 18:08:43'),(20,'Dont Breath','blind man kills everyone',2016,'English',88,'http://www.impawards.com/2016/posters/dont_breathe_xlg.jpg','https://www.youtube.com/watch?v=76yBTNDB6vU','2017-04-21 20:01:21','2017-04-21 20:01:21'),(21,'Get out','be carful of white neighborhoods',2017,'English',130,'https://cdn.traileraddict.com/content/universal-pictures/get-out-2017-4.jpg','https://www.youtube.com/watch?v=A2JbO9lnVLE','2017-04-21 20:02:28','2017-04-21 20:02:28'),(22,'The Texas ChainSaw Massacre','Crazy farmer kills everyone',1974,'English',90,'http://www.gstatic.com/tv/thumb/movieposters/9560/p9560_p_v8_aa.jpg','https://www.youtube.com/watch?v=UWjXERbOr70','2017-04-21 20:04:02','2017-04-21 20:04:02'),(23,'The Wicker man','Scottish people are scary',1973,'English',82,'http://t0.gstatic.com/images?q=tbn:ANd9GcTsjxZx32BCzIf6S_GO8AklduFxaEgM57rYy7qwLM15ssopwbGM','https://www.youtube.com/watch?v=G8tHgGncPA0','2017-04-21 20:05:03','2017-04-21 20:05:03'),(24,'Anchor Man','I love lamp',2004,'English',104,'https://upload.wikimedia.org/wikipedia/en/6/64/Movie_poster_Anchorman_The_Legend_of_Ron_Burgundy.jpg','https://www.youtube.com/watch?v=QQUi6Oi8SLQ','2017-04-21 20:07:21','2017-04-21 20:07:21'),(25,'Space Jam','MJ and Bugs Bunny ball on dem folks',1996,'English',100,'https://upload.wikimedia.org/wikipedia/en/1/14/Space_jam.jpg','https://www.youtube.com/watch?v=oKNy-MWjkcU','2017-04-21 20:10:00','2017-04-21 20:10:00'),(26,'2001: A Space Odyssey','The past and present are mixed together. ',1968,'English',162,'http://static.rogerebert.com/uploads/movie/movie_poster/2001-a-space-odyssey-1968/large_xA2zu0rqECkGIvgtzA3xXtQ24YC.jpg','https://www.youtube.com/watch?v=gSoGSeuxdTs','2017-04-21 20:11:05','2017-04-21 20:11:05'),(27,'Conan the Barbarian','Dont mess with Arnold Scwartzinager',1982,'English',129,'http://static.rogerebert.com/uploads/movie/movie_poster/conan-the-barbarian-1982/large_xcOdfPQqIKl6B7neTtDWDL5uFHK.jpg','https://www.youtube.com/watch?v=J6WslsAHKnA','2017-04-21 20:12:14','2017-04-21 20:12:14'),(28,'The Help','Shouldnt have made that pie',2011,'English',146,'https://upload.wikimedia.org/wikipedia/en/thumb/e/ef/Thehelpbookcover.jpg/220px-Thehelpbookcover.jpg','https://www.youtube.com/watch?v=pCtxIVcB6os','2017-04-21 20:13:13','2017-04-21 20:13:13'),(29,'Snakes on a plane','Whoever thought it was a good idea to bring snakes on a plane should never fly again',2006,'English',106,'https://upload.wikimedia.org/wikipedia/en/thumb/4/41/SOAP_poster.jpg/220px-SOAP_poster.jpg','https://www.youtube.com/watch?v=pCtxIVcB6os','2017-04-21 20:14:29','2017-04-21 20:14:29'),(30,'Friday','Ice cube had a fun friday',1995,'',122,'http://icecube.com/wp-content/uploads/2015/03/friday-movie-poster.jpg','https://www.youtube.com/watch?v=umvFBoLOOgo','2017-04-21 20:15:26','2017-04-21 20:15:26'),(31,'The Parent Trap','Lindsey Lohan has a twin sister and they get into some trouble',1998,'English',128,'https://img.buzzfeed.com/buzzfeed-static/static/2015-12/11/11/enhanced/webdr09/enhanced-13511-1449852129-7.jpg','https://www.youtube.com/watch?v=_7QQCf139fo','2017-04-21 20:16:40','2017-04-21 20:16:40'),(32,'Shindlers List','gotta save my workers',1993,'English',195,'https://images-na.ssl-images-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg','https://www.youtube.com/watch?v=XNSsv86lsok','2017-04-21 20:17:49','2017-04-21 20:17:49'),(33,'Pulp Fiction','Say what again!',1994,'English',176,'https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMTA5OTAzMl5BMl5BanBnXkFtZTgwNjA5MDc3NjE@._V1_UX182_CR0,0,182,268_AL_.jpg','https://www.youtube.com/watch?v=Mnb_3ibUp38','2017-04-21 20:18:45','2017-04-21 20:18:45'),(34,'Cars','Cars are anmiated and they race each other.',2006,'English',117,'https://images-na.ssl-images-amazon.com/images/I/51YP4C-LjhL.jpg','https://www.youtube.com/watch?v=h3RYTbJr07M','2017-04-21 20:19:42','2017-04-21 20:19:42');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) DEFAULT NULL,
  `review` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (1,5,'The best movie ever created.','2017-04-21 00:03:20','2017-04-21 00:03:20'),(22,3,'Eh.','2017-04-21 14:37:35','2017-04-21 14:37:35'),(23,5,'Cinematically Thrilling!','2017-04-21 14:38:56','2017-04-21 14:38:56'),(24,5,'Donkey!!! hilarious!','2017-04-21 14:39:58','2017-04-21 14:39:58'),(25,5,'Another Prequel!','2017-04-21 14:44:01','2017-04-21 14:44:01'),(26,2,'A night with dark with a knight.','2017-04-21 14:44:33','2017-04-21 14:44:33'),(27,5,'TOBY MCGUIRE! MY MANN!!','2017-04-21 14:45:11','2017-04-21 14:45:11'),(28,5,'Jamzz in Space Dude.','2017-04-21 14:45:28','2017-04-21 14:45:28'),(29,5,'Scotch! Scotch! Scotch! hahah!','2017-04-21 14:45:58','2017-04-21 14:45:58'),(30,5,'I have never been more happy to watch a movie in my life.','2017-04-21 14:46:25','2017-04-21 14:46:25'),(31,5,'Great for falling asleep to','2017-04-21 14:49:51','2017-04-21 14:49:51'),(32,4,'great movie','2017-04-21 15:26:15','2017-04-21 15:26:15'),(33,3,'This story is starting to get old.','2017-04-21 21:06:31','2017-04-21 21:06:31'),(34,2,'Shia isn\'t very good.','2017-04-21 21:07:02','2017-04-21 21:07:02'),(35,3,'Decent. ','2017-04-21 21:07:34','2017-04-21 21:07:34'),(36,2,'Chris Pratt carries this film.','2017-04-21 21:07:59','2017-04-21 21:07:59'),(37,1,'Terrible plot but RIP Paul Walker.','2017-04-21 21:08:34','2017-04-21 21:08:34'),(38,5,'HERES JOHNNY!','2017-04-21 21:08:57','2017-04-21 21:08:57'),(39,1,'im scurred','2017-04-21 21:09:30','2017-04-21 21:09:30'),(40,3,'Pretty entertaining.','2017-04-21 21:09:51','2017-04-21 21:09:51'),(41,3,'Pretty scary','2017-04-21 21:10:11','2017-04-21 21:10:11'),(42,3,'Beware','2017-04-21 21:10:36','2017-04-21 21:10:36'),(43,5,'One of the greatest comedies of the ages.','2017-04-21 21:11:06','2017-04-21 21:11:06'),(44,5,'BUGS BUNNY FTW','2017-04-21 21:11:30','2017-04-21 21:11:30'),(45,5,'Dont be Dave','2017-04-21 21:11:51','2017-04-21 21:11:51'),(46,4,'Arnold plays Conan O\'brien','2017-04-21 21:12:30','2017-04-21 21:12:30'),(47,1,'halp.','2017-04-21 21:13:02','2017-04-21 21:13:02'),(48,1,'Classic Samuel L.','2017-04-21 21:13:29','2017-04-21 21:13:29'),(49,3,'Saturday is better.','2017-04-21 21:13:54','2017-04-21 21:13:54'),(50,3,'Wait, there\'s two of them?','2017-04-21 21:14:20','2017-04-21 21:14:20');
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Producer','2017-04-20 22:48:28','2017-04-20 22:48:28'),(2,'Director','2017-04-20 22:48:42','2017-04-20 22:48:42'),(3,'Lighting','2017-04-20 22:48:57','2017-04-20 22:48:57'),(4,'Composer','2017-04-20 22:49:05','2017-04-20 22:49:05'),(5,'Screenwriter','2017-04-20 22:49:18','2017-04-20 22:49:18'),(6,'Make-up','2017-04-20 22:49:25','2017-04-20 22:49:25'),(7,'Actor','2017-04-20 22:49:34','2017-04-20 22:49:34'),(8,'Sound','2017-04-21 20:49:10','2017-04-21 20:49:10'),(9,'Executive Producer','2017-04-21 20:49:27','2017-04-21 20:49:27'),(10,'Cinematographer','2017-04-21 20:49:44','2017-04-21 20:50:08'),(11,'Writer','2017-04-21 20:50:20','2017-04-21 20:50:20'),(12,'Actress','2017-04-21 20:50:46','2017-04-21 20:50:46'),(13,'Assistant Producer','2017-04-21 20:51:05','2017-04-21 20:51:05'),(14,'Assistant Director','2017-04-21 20:51:21','2017-04-21 20:51:21'),(15,'Camera Operator','2017-04-21 20:51:43','2017-04-21 20:51:43'),(16,'Editor','2017-04-21 20:51:53','2017-04-21 20:51:53'),(17,'Legal','2017-04-21 20:52:47','2017-04-21 20:52:47');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20170417155842'),('20170417181658'),('20170417181740'),('20170417181943'),('20170419204342'),('20170419204426'),('20170419211302'),('20170419211405'),('20170420032518'),('20170420032538'),('20170420032624'),('20170420032653'),('20170420230732');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'kids movie','2017-04-20 22:42:41','2017-04-20 22:42:41'),(2,'explosions','2017-04-20 22:42:51','2017-04-20 22:42:51'),(3,'racing','2017-04-20 22:44:25','2017-04-20 22:44:25'),(4,'wizards','2017-04-20 22:44:32','2017-04-20 22:44:32'),(5,'witches','2017-04-20 22:44:41','2017-04-20 22:44:41'),(6,'Laser Sword','2017-04-21 14:43:17','2017-04-21 14:43:17'),(7,'scary girl','2017-04-21 18:05:34','2017-04-21 18:05:34'),(8,'Must-see','2017-04-21 20:53:21','2017-04-21 20:53:21'),(9,'New','2017-04-21 20:53:29','2017-04-21 20:53:29'),(10,'Old','2017-04-21 20:53:37','2017-04-21 20:53:37'),(11,'Terrible','2017-04-21 20:54:04','2017-04-21 20:54:04'),(12,'Rotten','2017-04-21 20:54:14','2017-04-21 20:54:14'),(13,'Fresh','2017-04-21 20:54:23','2017-04-21 20:54:23'),(14,'Stellar','2017-04-21 20:54:34','2017-04-21 20:54:34'),(15,'Date Night','2017-04-21 20:54:43','2017-04-21 20:54:43'),(16,'meh','2017-04-21 20:55:12','2017-04-21 20:55:12'),(17,'Superhero','2017-04-21 20:55:24','2017-04-21 20:55:24'),(18,'Violent','2017-04-21 20:55:33','2017-04-21 20:55:33'),(19,'Explicit','2017-04-21 20:55:44','2017-04-21 20:55:44'),(20,'Sports','2017-04-21 20:55:54','2017-04-21 20:55:54'),(21,'Heartwarming','2017-04-21 20:56:04','2017-04-21 20:56:04'),(22,'Feel-good','2017-04-21 20:56:19','2017-04-21 20:56:19'),(23,'Oscar nominated','2017-04-21 20:56:33','2017-04-21 20:56:33'),(24,'Oscar Winner','2017-04-21 20:56:43','2017-04-21 20:56:43'),(25,'Musical','2017-04-21 20:56:53','2017-04-21 20:56:53'),(26,'Classic','2017-04-21 20:57:16','2017-04-21 20:57:16'),(27,'Silent','2017-04-21 20:57:25','2017-04-21 20:57:25'),(28,'Black & White','2017-04-21 20:57:36','2017-04-21 20:57:36'),(29,'3D','2017-04-21 20:57:45','2017-04-21 20:57:45'),(30,'Sequel','2017-04-21 20:57:54','2017-04-21 20:57:54');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `permissions` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'mamu235@g.uky.edu','$2a$11$MP3xSR20M2cAbMI7hxGuae6SMWzCeC5l5OBtrkHuFtkChdbsnLq7O',NULL,NULL,NULL,10,'2017-04-21 17:27:21','2017-04-21 15:21:40','127.0.0.1','127.0.0.1','Mitch','A.','Mullins','4/26/1995','Male','--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\nManager: true\nAdmin: true\n','2017-04-20 22:32:21','2017-04-21 17:27:21'),(3,'mitch@mitch.com','$2a$11$x19oWgzFNbSaXh7E.4ire.9tlvmfW1U98RITGJK9MYDnF4dnFbALW',NULL,NULL,NULL,2,'2017-04-21 05:37:01','2017-04-21 05:12:13','127.0.0.1','127.0.0.1','WHOOO','','','','Male','--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\nManager: \'false\'\n','2017-04-21 05:12:13','2017-04-21 05:38:44'),(4,'sam@sam.com','$2a$11$8IlULmEEl1Hw071W9UgN5.pgwGRXKisLM6LgnsrsONAwJr6MZ1pme',NULL,NULL,NULL,1,'2017-04-21 05:47:12','2017-04-21 05:47:12','127.0.0.1','127.0.0.1','Sam','','','','Male',NULL,'2017-04-21 05:47:12','2017-04-21 05:47:12'),(5,'mlja226@uky.edu','$2a$11$7VACUyE8N4YOB.2.fYx13OjEfTNCNLRh3fyg3XJKlgTQEgisVfAdi',NULL,NULL,NULL,6,'2017-04-21 20:03:05','2017-04-21 17:09:22','76.10.52.36','76.10.52.21','Matt','L','Jackson','12','Male','--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\nManager: \'true\'\nAdmin: \'true\'\n','2017-04-21 13:16:39','2017-04-21 20:03:05'),(6,'matt.jackson1294@gmail.com','$2a$11$FCeqCtxgAsd1e1wDqpflVuxQOzEEe6/AvJQWfK1g3F1Bj1C0t/XOm',NULL,NULL,NULL,1,'2017-04-21 15:21:46','2017-04-21 15:21:46','128.163.239.239','128.163.239.239','Matt','ja','jackson','12/12/1994','Male','--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\nAdmin: \'false\'\n','2017-04-21 15:21:46','2017-04-21 15:29:38');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `watchlists`
--

DROP TABLE IF EXISTS `watchlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `watchlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `movie_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_watchlists_on_user_id` (`user_id`),
  KEY `index_watchlists_on_movie_id` (`movie_id`),
  CONSTRAINT `fk_rails_0dc1a4cbcb` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_rails_1de1911646` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `watchlists`
--

LOCK TABLES `watchlists` WRITE;
/*!40000 ALTER TABLE `watchlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `watchlists` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-21 17:30:28
